---
author: Lucas Oliveira
title: Contact
date: 2020-03-08
description: Página de Contato
contact: true
---

## Informações de Contato

- **Endereço:** Natal, Rio Grande do Norte - Brasil 🇧🇷
- **Telefone:** 55 (84) 99224-8571
- **E-mail:** luksjobs@live.com
- **Linkedin:** https://linkedin.com/in/luksjobs