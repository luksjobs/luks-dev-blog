+++
title = "Discord"
description = "Entre em meu canal do Discord e faça parte da comunidade onde eu tiro dúvidas e frequentemente posto coisas legais sobre o mundo de devops dentre outros assuntos sobre Tecnologia da Informação, jogos e outras coisas..."
date = "2022-04-10"
aliases = ["certifications"]
author = "Lucas Oliveira"
+++

Acesse meu canal do [Discord](https://www.discord.com/luksjobs)
