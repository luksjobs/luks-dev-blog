+++
title = "Twitch"
description = "Vez por outra estou online em meu canal do Twitch Stremando algum jogo ou então codando, aproveita que você está por aqui e me segue la :)"
date = "2022-10-07"
aliases = ["awards"]
author = "Lucas Oliveira"
+++

Meu canal do [Twitch](https://www.twitch.tv/luksgrota)

---

<html>
  <body>
    <!-- Add a placeholder for the Twitch embed -->
    <div id="twitch-embed"></div>

    <!-- Load the Twitch embed JavaScript file -->
    <script src="https://embed.twitch.tv/embed/v1.js"></script>

    <!-- Create a Twitch.Embed object that will render within the "twitch-embed" element -->
    <script type="text/javascript">
      new Twitch.Embed("twitch-embed", {
        width: 854,
        height: 480,
        channel: "luksgrota",
        // Only needed if this page is going to be embedded on other websites
        parent: ["embed.example.com", "othersite.example.com"]
      });
    </script>
  </body>
</html>